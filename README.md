This repository contains all the files associated with my master thesis written from august 2014 to june 2015 at NTNU, Norwegian University of Science and Technology.

All experiment data is located in the mecoboprober/data directory. The mecoboprober directory contains all the software used to obtain the data and to control Mecobo/optowall in general. 

The pcb sub module contains Altium Designer source and output files for the optowall and optoamp PCBs. 

The rest of the repository is probably not of interest: The uc submodule contains microcontroller software used in the early phases of the thesis, but with no direct relevance to the thesis; the report submodule contains the LaTeX source files for the report; the datasheets directory contains datasheets for off-the-shelf components; the logg directory contains some of my notes, logs, and meeting minutes. 
