def binlist2bin(binlist):
    return int(binlist2str(binlist), 2)

def binlist2str(binlist):
    s = ""
    for bit in reversed(binlist):
        s += str(bit)
    return s

def binlist2str_not_reversed(binlist):
    s = ""
    for bit in binlist:
        s += str(bit)
    return s

def bin2str(val, size):
    val = bin(val)[2:]
    return "0" * (size - len(val)) + val

def bin2list(val):
    return binstr2list(bin(val)[2:])

def binstr2list(val):
    """ 
    Assumes format "101010101"
    """
    return list(val)[::-1]

