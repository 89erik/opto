# mecoboprober
This directory contains all files used to perform the experiments documented in the report.

This root directory contains a few source code files that are shared by the subdirectories. The data itself is contained in the "data" directory. The software to gather the data, or experiment logs, is contained in the "gather_data" directory. This software will probe Mecobo in various ways and its output has been stored in files in the "data" directory. Software to analyze and plot the data is contained in the "analyze_data" directory.

All software has been written for **Python 2.7.6**

