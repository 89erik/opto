#!/usr/bin/python
import sys
import re

import matplotlib.pyplot as plt #sudo apt-get install python-matplotlib

try:
    filename = sys.argv[1]
except:
    print "usage: " + sys.argv[0] + " <filename>"
    exit(1)

def read_data():
    print "---["+filename+"]---"
    with open(filename, "r") as f:
        x = []
        y = []
        pattern = re.compile("([0-1]+) -> ([0-1])")
        line = f.readline()
        while line:
            m = re.match(pattern, line)
            if m:
                x.append(int(m.group(1), 2))
                y.append(int(m.group(2)))
                bin_len = len(m.group(1))
            else:
                sys.stdout.write(line)
            line = f.readline()

    sys.stdout.flush()
    return (x, y)

def plot_data(x,y):
    plt.plot(x, y)
    plt.fill_between(x,y)
    
    locs,labels = plt.xticks()
    plt.xticks(locs, map(lambda x: bin(int(x)), locs))

    plt.xlabel("Input")
    plt.ylabel("Output")
    plt.axis([0,len(x), 0, 1])
    plt.title(filename)
    plt.show()

x,y = read_data()
plot_data(x,y)

