#!/bin/sh

for arg in $@
do
    python $(dirname $0)/log_displayer.py $arg > /dev/null &
done
