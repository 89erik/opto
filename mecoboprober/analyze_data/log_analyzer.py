#!/usr/bin/python
import sys
import re
import json
import os
import traceback
from math import sqrt

try:
    import matplotlib.pyplot as plt #sudo apt-get install python-matplotlib
    plotlib = True
except:
    print " -----------------------------------------------------------------"
    print "|       WARNING: matplotlib not installed, plots will fail!       |"
    print "| Install on ubuntu with 'sudo apt-get install python-matplotlib' |"
    print " -----------------------------------------------------------------"
    plotlib = False


sys.path.append("..")
from binstrings import binlist2str, binstr2list, bin2str, binlist2bin
from util import *

N_PINS = 16


try:
    filename = sys.argv[1]
except:
    print "usage: " + sys.argv[0] + " <filename>"
    exit(1)

class LogFile():

    def __init__(self):
        self.file_position = 0
        self._data = None

    def reset(self):
        self.file_position = 0
    
    def _read_data_group(self):
        """
        Reads one data group from file and returns xs and ys as separate lists.
        One data group is the data of a sweep for one output pin.
        Also returnes the group divisor and a list of indexes (to xs and ys) for unstable probes.
        """
        if self.file_position == 0:
            print "---["+filename+"]---"
        elif self.file_position == -1:
            print "EOF"
            return (None, None, None, None)

        with open(filename, "r") as f:
            f.seek(self.file_position)
            xs = []
            ys = []
            unstables = []
            n_lines = 0
            data_pattern    = re.compile("([0-1]{%d}) -> ([0-1](\.[0-9]+)?)[\n\r]+$" % (N_PINS-1))
            divisor_pattern = re.compile("input=\[[^[]*\], config=\[[^[]*\], output=\[[^[]*\]")

            def skip_to_pattern(pattern):
                line = f.readline()
                while line:
                    match = re.match(pattern, line)
                    sys.stdout.write("(skipping)    " + line)
                    prevline = line
                    line = f.readline()
                    if match: return prevline, line
                raise Exception("Encountered EOF during skip")
            divisor, line = skip_to_pattern(divisor_pattern)

            while line:
                m = re.match(data_pattern, line)
                if m:
                    x = m.group(1)
                    y = m.group(2)
                    if "." in y:
                        if not (y == "1.0" or y == "0.0"):
                            unstables.append(len(xs)) #this must happen before x and y are added to lists
                        y = str(int(round(float(y))))
                    assert y == "0" or y == "1"

                    xs.append(binstr2list(x))
                    ys.append([y])

                elif re.match(divisor_pattern, line):
                    self.file_position = f.tell() - len(line)
                    break
                else:
                    sys.stdout.write("(no match)    " + line)
                line = f.readline()
                n_lines += 1

            if not line:
                self.file_position = -1

            n_unstables = len(unstables)
            unstable_percentage = (float(n_unstables) / float(n_lines)) * 100
            print "%d out of %d probes where unstable (%f %%)" % (n_unstables, n_lines, unstable_percentage)

        sys.stdout.flush()
        return (xs, ys, divisor, unstables)

    def count_probe_outputs(self):
        counts = {"0": 0, "1": 0}
        for d in self.data.values():
            for y in d["ys"]:
                y = y[0]
                counts[y] += 1
        return counts

    @staticmethod
    def divisor2dict(divisor):
        """
        Return a divisor string as a dictionary
        """
        if divisor.endswith("\n"):
            divisor = divisor[:-1]
        pin_conf = {}
        while divisor:
            # Extract and remove group from divisor string
            if divisor.startswith(", "):
                divisor = divisor[len(", "): ]
            pin_group = re.match("[^=]+=\[[^]]*\]", divisor).group(0)
            divisor = divisor[len(pin_group): ]

            # Place group content into dictionary
            name = re.match("[^=]+", pin_group).group(0)
            pin_conf[name] = pin_group[len(name+"=["):-1].split(", ")

        assert len(pin_conf) == 3
        assert len(pin_conf["input"]) == 1
        assert pin_conf["input"][0] == ""
        assert len(pin_conf["output"]) == 1
        assert len(pin_conf["config"]) == N_PINS-1

        pin_conf["input"] = []
        for i in pin_conf:
            map(int, pin_conf[i])
        return pin_conf

    @staticmethod
    def real_pin_conf(divisor, inputs):
        """
        Returns a string describing pin configuration based on group separator from log file 
        and array of input pins
        """
        pin_conf = LogFile.divisor2dict(divisor)
        
        pin_conf["input"] = ["", ""]
        for i, pin in enumerate(inputs):
            # Using pin number as index to config vector
            pin_conf["input"][i] = pin_conf["config"].pop(pin-i)

        return str(pin_conf).replace("u'","").replace("'", "")

    def get_group(self, output_pin):
        """
        Returns data group given by output_pin argument
        """
        divisor = None
        for d in self.data:
            print "divisor = %s" % d
            do = LogFile.divisor2dict(d)["output"]
            print "d[\"output\"]: %s "% do
            assert len(do) == 1
            if int(do[0]) == output_pin:
                divisor = d
                break
        if not divisor:
            return None
        print self.data[divisor]["xs"][:10]
        print self.data[divisor]["ys"][:10]
        return self.data[divisor]

    @property
    def data(self):
        if not self._data:
            self._data = {}
            xs, ys, divisor, unstables = self._read_data_group() 
            while (xs):
                assert ys and divisor
                self._data[divisor] = {"xs": xs, "ys": ys, "unstables": unstables}
                xs, ys, divisor, unstables = self._read_data_group()
            self.reset()
        return self._data
### END CLASS LogFile ###


class GateSumData():
    
    def __init__(self, cache, log_file):
        self._data = None
        self.cache = cache
        self.log_file = log_file

    def calculate_pin_configurations(self):
        """ 
        Calculates all possible pin configurations and stores to a global variable.
        A pin configuration is represented by two numbers specifying input pins.
        """
        self.all_pin_configurations = []
        for in2 in range(N_PINS-1):
            for in1 in range(N_PINS-1):
                if in1 >= in2: break
                self.all_pin_configurations.append([in1, in2])

    @staticmethod
    def sort_by_material_config(xs, pin_configuration):
        """ 
        Takes one pin configuration and groups all equal material configuration 
        values together. The result is formated as a dictionary with material 
        configuration as key. The value is a list of pointers to rows that has 
        that material configuration.
        """
        config_pins = set(range(N_PINS-1)) - set(pin_configuration)
        material_configs = {}
        
        for i, x in enumerate(xs):
            material_config = tuple([x[j] for j in config_pins])

            dict_list_append(material_configs, material_config, i)

        return material_configs

    def calculate_gate_sums(self, xs, ys, unstables_raw):
        """
        Takes a group of data and finds all gate sums for any configuration.
        This is done by, for all pin configurations, finding all four rows that 
        share configuration, and producing a gate of their combined output.
        """
        plot_xs = []
        plot_ys = []
        pin_confs = []
        unstable_xs = []
        unstable_ys = []

        def format_gate_sum(gs):
            return int(binlist2str(gs[("1","1")] + gs[("1","0")] + gs[("0","1")] + gs[("0","0")]), 2)
        def format_mat_conf(mat_conf):
            return int(binlist2str(mat_conf), 2)

        gate_sums_ok = True

        for pin_configuration in self.all_pin_configurations:
            if not gate_sums_ok: break
            material_config_map = GateSumData.sort_by_material_config(xs, pin_configuration)
            for mat_conf, pointers in material_config_map.iteritems():
                if len(pointers) != 4: 
                    print "Mat conf does not have four instances. Gate sums invalid!"
                    gate_sums_ok = False
                    break
                gs = {}
                unstable_gs = {}
                for i in pointers:
                    inputs = [xs[i][in_bit] for in_bit in pin_configuration]
                    gs[tuple(inputs)] = ys[i]
                    if i in unstables_raw:
                        # For unstable input, save opposite output to an unstable gate sum
                        unstable_gs[tuple(inputs)] = [str(1 - int(ys[i][0]))]
                
                for ui in unstable_gs: # For all unstable inputs in unstable gate sum
                    u_gs = gs.copy()    
                    u_gs[ui] = unstable_gs[ui] # create alternative gs
                    unstable_xs.append(format_mat_conf(mat_conf))
                    unstable_ys.append(format_gate_sum(u_gs))

                plot_xs.append(format_mat_conf(mat_conf))
                plot_ys.append(format_gate_sum(gs))
                pin_confs.append(pin_configuration)

        return {"xs": plot_xs, 
                "ys": plot_ys, 
                "pin_confs": pin_confs, 
                "unstable_xs": unstable_xs, 
                "unstable_ys": unstable_ys,
                "n_unstable_probes": len(unstables_raw)}

    @staticmethod
    def configurations(logic_gate, ys):
        """
        Returns pointers to the configurations that results in logic gate
        """
        configs = []
        for i in range(len(ys)):
            if ys[i] == logic_gate:
                configs.append(i)
        return configs

    def load_cache(self):
        sys.stdout.write("Loading gate sum data from cache...")
        sys.stdout.flush()
        with open(filename+".gate_sum_cache", "r") as f:
            self._data = json.loads(f.read())
        sys.stdout.write(" done\n")
        sys.stdout.flush()

    def store_cache(self):
        with open(filename+".gate_sum_cache", "w") as f:
            f.write(json.dumps(self.data))

    @property
    def data(self):
        if not self._data:
            if os.path.isfile(filename+".gate_sum_cache") and self.cache:
                self.load_cache()
            else:
                print "Loading gate sum data, this may take a while."
                self._data = {}
                all_unstables = []

                self.calculate_pin_configurations()
                for divisor, log_data in self.log_file.data.items():
                    xs = log_data["xs"]
                    ys = log_data["ys"]
                    unstables = log_data["unstables"]
                    self._data[divisor] = self.calculate_gate_sums(xs, ys, unstables)
                self.store_cache()
                alert("Gate sum data ready")
        return self._data
### END CLASS GateSumData ###

def run_prompt(log_file, gate_sums):
    """
    This function is called with all log file data in the group_data 
    dictionary argument. It presents a prompt to the user and contains 
    all necessary subfunctions to perform the user's commands (except 
    for plot_data which is global). 
    """
    include_unstables = True
    balanced = False

    def p_help():
        s = "'help', 'h', and '?' displays this help\n"
        s+= "'q' exits\n"
        s+= "'filename' prints the filename\n"
        s+= "'plot' plots the gate sums\n"
        s+= "'simple_plot' <groups> XY plot of single group(s)\n"
        s+= "'state' prints state variables\n"
        s+= "'count' prints a count of all gates\n"
        s+= "'count x y z' prints a count of gate x, y and z (optional amount of args)\n"
        s+= "'output_count', 'output_stability', and 'b' counts the number of '1's and '0's in the probe outputs\n"
        s+= "'functionality' and 'f' determines the functionality of sweep\n"
        s+= "'stability' and 's' prints the number of unstable probes\n"
        s+= "'input_differences' and 'd' prints the input differences of unstable probes\n"
        s+= "'mv_vs_s' and 'mvs' prints a plot of relationship between d(v) and S\n"

        s+= "Flip a state variable by typing its name\n"
        s+= "Type a gate sum in decimal for its configurations, add filename to write to file\n"
        return s

    def state_variables():
        s= "include_unstables: %s\n" % include_unstables
        s+="balanced: %s\n" % balanced
        return s

    def ints_from_user(from_user, default=range(0,16)):
        args = from_user[1:]
        if args:
            try:
                return map(int, args)
            except ValueError:
                print "bad arguments: " + str(args)
                return []
        else:
            return default

    def gate_count(gs=None):
        """
        Counts the instances of all gates
        """
        if not gs:
            if include_unstables:
                gs = [y for g in gate_sums.data.values() for y in g["ys"] + g["unstable_ys"]]
            else:
                gs = [y for g in gate_sums.data.values() for y in g["ys"]]

        gate_count = [0] * 16

        for gate in gs:
            gate_count[gate] += 1
        return gate_count

    def functionality(ys):
        """
        Calculates the functionality rating
        """
        ns = gate_count(ys)
        mean = average(ns)
        avg_diffs = map(lambda c: abs(mean - c), ns)
        avg_diff = average(avg_diffs)

        gates_available = 0
        for gate in ns:
            if gate: gates_available += 1

        print "avg_diff: " +str(avg_diff)
        print "1.0/avg_diff: %f" % (1.0/avg_diff)
        print "gates_available: %d" % gates_available
        return (200000.0/ avg_diff) * gates_available


    def unstable_probes(): 
        """
        Returns the input vectors of all unstable probes
        """
        l = []
        for log_data in log_file.data.values():
            xs = log_data["xs"]
            unstables = log_data["unstables"]
            l.extend([xs[i] for i in unstables])
        return l

    def input_difference(x):
        """ 
        Calcuates the total difference of potential of an input vector.
        This function is reffered to as "d" in the report.
        """
        x = map(int, x)
        mean = average(x)
        return -2 * sqrt(pow(mean - 0.5, 2))+1

    def mv_vs_s():
        """
        Prints a plot of relationship between d(v) and S
        """
        plot = {}

        for group_data in log_file.data.itervalues():
            ds = map(lambda x: average(map(int,x)), group_data["xs"])
            ss = map(lambda i: 0 if i in group_data["unstables"] else 1, range(len(group_data["xs"])))

            assert len(ds) == len(ss)
            for i in range(len(ds)):
                dict_list_append(plot, ds[i], ss[i])
        xs = []
        ys = []
        for x, y in sorted(plot.iteritems()):
            xs.append(x)
            ys.append(average(y))
        print latex_plot(xs,ys)
        #simple_plot(xs,ys)

    def find_gates(gate_sum, filepath, return_xs=False):
        def report(s):
            if filepath:
                with open(filepath, "w") as f:
                    f.write(s)
            else:
                MAX = 10000
                if len(s) > MAX:
                    print s[:MAX]
                    print "%d characters remain." % (len(s)-MAX)
                    cont = raw_input("Continue? [y/N] ")
                    if cont.upper() == "Y":
                        print s[MAX:]
                else:
                    print s

        s = ""
        xs = []
        for divisor in gate_sums.data:
#            xs, ys, pin_confs = group_plots[divisor]
            group = gate_sums.data[divisor]
            confs = GateSumData.configurations(gate_sum, group["ys"])
            if include_unstables and "unstable_ys" in group:
                unstable_confs = GateSumData.configurations(gate_sum, group["unstable_ys"])
                for i in unstable_confs:
                    x = group["unstable_xs"][i]
                    xs.append(x)
                    pin_conf = LogFile.real_pin_conf(divisor, group["pin_confs"][i])
                    s += pin_conf + ": "+bin(x)+" (unstable)\n"
            for i in confs:
                x = group["xs"][i]
                xs.append(x)
                pin_conf = LogFile.real_pin_conf(divisor, group["pin_confs"][i])
                s += pin_conf + ": "+bin(x)+"\n"
        if return_xs: 
            return xs
        elif s:
            report("Configurations for " + bin2str(gate_sum, 4) + ":\n"+ s)
        else:
            print "Gate not available"


    print p_help()
    while True:
        from_user = raw_input(">").split(" ")
        command = from_user[0]
        if command == "quit" or command == "q" or command == "exit":
            break
        elif command == "help" or command == "h" or command == "?":
            print p_help()
        elif command == "filename":
            print filename
        elif command == "plot":
            plot_data(gate_sums, include_unstables)
        elif command == "state":
            print state_variables()
        elif command == "include_unstables":
            include_unstables = not include_unstables
            print "include_unstables = " + str(include_unstables)
        elif command == "balanced":
            balanced = not balanced
            print "balanced = %s" % balanced
        elif command == "functionality" or command == "f":
            if False and include_unstables:
                gates = [y for g in gate_sums.data.values() for y in g["ys"] + g["unstable_ys"]]
            else:
                gates = [y for g in gate_sums.data.values() for y in g["ys"]]
            if include_unstables: print "overriding include_unstables"
            print "F = %f" % functionality(gates)
        elif command == "stability" or command == "s":
            try:
                n_unstable_probes = sum([g["n_unstable_probes"] for g in gate_sums.data.values()])
                print "%d probes are unstable" % n_unstable_probes
                if n_unstable_probes:
                    print "S = %f" % (50.0 / n_unstable_probes)
            except KeyError:
                print "Error: Outdated cache, rerun with '--no_cache' argument"
        elif command == "input_differences" or command == "d":
            diffs = []
            for p in unstable_probes():
                diffs.append(input_difference(p))
            
            ys = [0] * 10

            for d in diffs:
                ys[int(d * 10)] += 1
            
            xs = map(lambda x: x/10.0, range(10))
            for i in range(10):
                print "%f: %d" % (xs[i], ys[i])
            print "Average: %f" % average(diffs)
#            simple_plot(xs, ys)
        elif command == "df":
            gates = ints_from_user(from_user)
            for i in gates:
                xs = find_gates(i, None, return_xs=True)
                if xs:
                    xs = map(lambda x: bin(x)[2:], xs)
                    ds = map(input_difference, xs)
                    D = average(ds)
                    print "Gate %d: %f" % (i, D)
                else:
                    print "Gate %d is not available" % i
        elif command == "count":
            gates = ints_from_user(from_user)
            gate_sums_sum = 0
            for i, c in enumerate(gate_count()):
                if i in gates:
                    gate_sums_sum += c
                    print "Gate %d: %d instances" % (i, c)
            print "Total amount of gate sums: %d" % gate_sums_sum
        elif command == "simple_plot":
            groups = ints_from_user(from_user, default=[0])
            print "Plotting groups %s, one at a time" % groups
            for g in groups:
                g_data = log_file.get_group(g)
                if not g_data:
                    print "Group %d not found" % g
                    continue
                xs = map(binlist2bin, g_data["xs"])
                ys = map(binlist2bin, g_data["ys"])
                print "xs = %s" % xs[:100]
                print "ys = %s" % ys[:100]
                simple_plot(xs, ys)
            
        elif command == "output_count" or command == "output_stability" or command == "b":
            po = log_file.count_probe_outputs()
            print po
            print "Balance: %f" % (float(po["1"]) / float(po["1"]+po["0"]))
        elif command == "mv_vs_s" or command == "mvs":
            mv_vs_s()
        else:
            try:
                gate_sum = int(command)
            except:
                print "Does not understand input %s, with command '%s'" % (from_user, command)
                continue
            if len(from_user) == 2:
                filepath = from_user[1]
            else:
                filepath = None
            find_gates(gate_sum, filepath)

def latex_plot(xs, ys):
    """
    Returns a string of given plot data in latex pgfplots format
    """
    assert len(xs) == len(ys)
    s = "    \\addplot [mark=*, color=blue] coordinates { \n"
    for i in range(len(xs)):
        s+= "        (%s, %s)\n" % (str(xs[i]), str(ys[i]))
    s += "    };"
    return s

def simple_plot(xs, ys, xlabel="", ylabel="", fill=False, binary_xticks=False):
    if not plotlib: 
        print "Plot library not installed, cannot plot!"
        return
    plt.plot(xs, ys)
    if fill:
        plt.fill_between(x,y)
    if binary_xticks:
        locs,labels = plt.xticks()
        plt.xticks(locs, map(lambda x: bin(int(x)), locs))

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
#    plt.axis([0,len(x), 0, 1])
#    plt.title(filename)
    plt.show()

def plot_data(gate_sums, include_unstables):
    """
    Shows plot GUI
    """
    if not plotlib: 
        print "Plot library not installed, cannot plot!"
        return
    for group in gate_sums.data.values():
        if include_unstables:
            plt.scatter(group["unstable_xs"], group["unstable_ys"], s=0.1, color="red")
        plt.scatter(group["xs"], group["ys"], s=0.1)
    
    plt.xlabel("Configuration")
    plt.ylabel("Gate output sum")
    plt.axis([0,pow(2,N_PINS-3), 0, N_PINS])
#    plt.title(filename)
    plt.show()



def main():
    global N_PINS, no_cache

    plot_first = False
    cache = True
    options = sys.argv[2:]
    log_file = LogFile()
    if "--plot" in options:
        print "Plotting data, including unstables... (bypasses prompt)"
        plot_first = True
    if "--no_cache" in options or "--ignore_cache" in options:
        print "Ignoring any existing cache"
        cache = False
    if "--unstable_count" in options:
        print "Only reads log file (prints count)"
        _ = log_file.data
        exit(0)
    if "--output_count" in options:
        print count_probe_outputs()
        exit(0)

    n_pins = [s for s in options if "N_PINS=" in s]
    if n_pins:
        assert len(n_pins) == 1
        n_pins = n_pins[0].split("=")
        assert len(n_pins) == 2
        N_PINS = int(n_pins[1])
        print "N_PINS = %d" % N_PINS

    gate_sums = GateSumData(cache, log_file)
    run_prompt(log_file, gate_sums)

main()

