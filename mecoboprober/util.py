import subprocess

def alert(msg):
    try:
        subprocess.call(["/home/erik/.scripts/alert.sh", msg])
    except: 
        pass

def dict_list_append(dictionary, label, item):
    if not label in dictionary:
        dictionary[label] = [item]
    else:
        dictionary[label].append(item)

def average(l): 
    """
    Returns the average of a list
    """
    return sum(l) / float(len(l))

def format_ms(ms):
    h = ms / (60*60*1000)
    ms -= h*60*60*1000
    m = ms / (60*1000)
    ms -= m*60*1000
    s = ms / 1000
    ms -= s*1000
    return str(h) + "h " + str(m) + "m " + str(s) + "s " + str(ms) + "ms"

