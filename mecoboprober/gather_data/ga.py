import thrift_client as mecobo

import sys
import random
import time
from binstrings import *
from constants import *

POPULATION_SIZE = 5
N_GENERATIONS = 100000000
MUTATION_PROBABILITY = 1000

def chance(p):
    return random.randrange(0, p) == 0

if DEBUG:
    def debug(str):
        print(str)
else:
    def debug(str):
        pass

class Individual():

    PIN_CONF_GENE_SIZE = 4
    CONFIG_GENE_SIZE   = 7

    PIN_CONF_GENES = (0                , PIN_CONF_GENE_SIZE * (N_INPUTS + N_OUTPUTS))
    CONFIG_GENES   = (PIN_CONF_GENES[1] , PIN_CONF_GENES[1] + N_CONFIGS*CONFIG_GENE_SIZE)
    SIZE           = CONFIG_GENES[1]

    def __init__(self, genome=None):
        self._fitness = None
        if genome:
            self.genome = genome
            return
        
        def generate_pin_conf_genes():
            available_pins = range(0, N_PINS)
            inputs  = []
            outputs = []
            configs = []

            for i in range(0, N_INPUTS + N_OUTPUTS):
                pin = random.randrange(0, len(available_pins))
                if len(inputs) < N_INPUTS:
                    inputs.append(available_pins[pin])
                    del available_pins[pin]
                elif len(outputs) < N_OUTPUTS:
                    outputs.append(available_pins[pin])
                    del available_pins[pin]


            def to_pin_conf_format(pin_list):
                conf_list = [bin2str(pin, 4) for pin in pin_list]
                return binlist2str_not_reversed(conf_list)

            inputs  = to_pin_conf_format(inputs)
            outputs = to_pin_conf_format(outputs)
            return inputs + outputs
        
        def generate_config_genes():
            config_genes_length = Individual.CONFIG_GENES[1] - Individual.CONFIG_GENES[0]
            r = random.randrange(0, pow(2, config_genes_length))
            return bin2str(r, config_genes_length)

        self.genome = generate_pin_conf_genes() + generate_config_genes()
        assert len(self.genome) == Individual.SIZE

    def legal(self):
        """
        Returns whether this individual has a legal genome.
        """
        if len(self.genome) != Individual.SIZE: 
            return False
        g = self.genes_decoded()
        pins = g["input_pins"] + g["output_pins"] + g["config_pins"]
        if len(pins) != len(set(pins)):
            return False
        if N_CONFIGS != len(g["config_pins"]):
            return False
        return True

    def genes_encoded(self): 
        """
        Returns a dictionary containing all (encoded) genes of the genome.
        """
        ins    = (0, 4*N_INPUTS)
        outs   = (ins[1],  ins[1]  + 4*N_OUTPUTS)
        config = outs[1]
        return {"input_pins":    self.genome[ins[0]   :   ins[1]],
                "output_pins":   self.genome[outs[0]  :  outs[1]],
                "configuration": self.genome[config :]}

    def genes_decoded(self):
        """
        Returns the decoded genes.
        """
        def decode_pin_conf(gstr):
            decoded = []
            for i in range(0, len(gstr), 4):
                decoded.append(int(gstr[i:i+4], 2))
            return decoded
        def decode_config(gstr):
            freqs = []
            for i in range(0, len(gstr), 7):
                gval = int(gstr[i:i+7], 2)
                freqs.append(gval * (20000/127) + 400)
            return freqs


        g = self.genes_encoded()
        for name in g:
            if name == "configuration":
                g[name] = decode_config(g[name])
            else:
                g[name] = decode_pin_conf(g[name])
        g["config_pins"] = list(set(range(N_PINS)) - set(g["input_pins"] + g["output_pins"]))

        return g

    def mutate(self, untill_legal=False):
        i = random.randrange(0, Individual.SIZE)
        mutated_genome = self.genome[:i] + str(1 - int(self.genome[i])) + self.genome[i+1:]
        
        if untill_legal and not Individual(mutated_genome).legal():
            print "mutation failed, trying again"
            self.mutate(untill_legal = True)
        else:
            self.genome = mutated_genome

        self._fitness = None
    
    def mate(self, other):
        c1 = random.randrange(*Individual.PIN_CONF_GENES)
        c2 = random.randrange(*Individual.CONFIG_GENES)

        assert c1 < c2

        r1 = self.genome[:c1] + other.genome[c1:c2] + self.genome[c2:]
        r2 = other.genome[:c1] + self.genome[c1:c2] + other.genome[c2:]
    
        r1 = Individual(r1)
        r2 = Individual(r2)
        if chance(MUTATION_PROBABILITY): r1.mutate()
        if chance(MUTATION_PROBABILITY): r2.mutate()

        return (r1, r2)
    
    @property
    def fitness(self):
        """
        Returns fitness. If necessary, calculates fitness by 
        probing Mecobo with all 2^2 possible inputs.
        """
        if not self._fitness:
            genes = self.genes_decoded()

            def probe(input):
                mecobo.queue_value(pow(2, N_CONFIGS)-1, genes["config_pins"], wave=True, frequencies=genes["configuration"])
                mecobo.queue_value(input, genes["input_pins"], wave=False)
                mecobo.queue_record_sequences(genes["output_pins"])
                mecobo.run_sequences()
                waves = mecobo.get_recordings(wave=True)
                assert len(waves) == 1
                wave = waves[0]
                return float(sum(wave)) / float(len(wave))

            def score(input, output):
                if input == 0b00:
                    w = 0.3
                    expects = 0
                elif input == 0b01:
                    w = 0.5
                    expects = 1
                elif input == 0b10:
                    w = 0.5
                    expects = 1
                elif input == 0b11:
                    w = 1.15
                    expects = 0

                if expects == 1:
                    correctness = output
                else:
                    correctness = 1.0 - output
                return correctness * w

            self._fitness = 0
            for input in range(0,0b11+1):
                output = probe(input)
                s = score(input, output)
                self._fitness += s
                debug(bin(input)[2:]+" -> " + str(output) + ", score: %f" % s)
            debug("fitness: %f" % self._fitness)

        return self._fitness


    @fitness.setter
    def fitness(self, value):
        raise Exception()
    @fitness.deleter
    def fitness(self):
        raise Exception()

    def copy(self):
        return Individual(self.genome)

    def __str__(self):
        g = self.genes_decoded()

        return "Fitness: %f, Genes: %s" % (self.fitness, str(g))

    def __cmp__(self, other):
        if self.fitness < other.fitness:
            return 1
        elif self.fitness > other.fitness:
            return -1
        else:
            return 0
# --[END CLASS]-- #
        

def next_generation_complex(population, generation_index):
    """ 
    Uses fitness, selection and crossover to 
    generate and return the next generation.
    """

    def select_elite(k):
        assert k > 0 and k <= len(population)
        population.sort()
        return population[:k]

    def select_by_roulette():
        map_index = random.randrange(0, int(total_fitness))

        k = 0
        for i in range(POPULATION_SIZE):
            k += population[i].fitness
            if k >= map_index:
                return population[i]

    def select_by_tournament():
        k = 5
        best = None
        for i in range(k):
            indv = population[random.randrange(0, len(population))]
            if 0 == i or indv.fitness > best.fitness:
                best = indv
        return best        
    
    next_generation = select_elite(5)
    for indv in next_generation[:10]:
        print indv
    print next_generation[0]
    while len(next_generation) < POPULATION_SIZE:
        indv1 = select_by_tournament()
        indv2 = select_by_tournament()
        children = indv1.mate(indv2)
        for child in children:
            if child.legal(): next_generation.append(child)

    assert len(next_generation) >= POPULATION_SIZE 
    return next_generation[:POPULATION_SIZE]

def next_generation_simple(population, generation_index):
    population.sort()
    best = population[0]

    if len(population) > 1:
        print "Generation %d\n%s" % (generation_index, str(best))
            
    next_generation = []
    for i in range(len(population)):
        indv = best.copy()
        indv.mutate(untill_legal=True)
        next_generation.append(indv)
    
    next_generation.sort()
    if best.fitness <= next_generation[0].fitness:
        best = next_generation[0]
        print "Generation %d\n%s" % (generation_index, str(best))
    
    return [best]

def initialize():
    global start_time
   
    start_time = time.time()
    random.seed(start_time)
    
    mecobo.init_connection()

def terminate():
    global start_time
    delta_t = time.time() - start_time
    print "Total time elapsed: %s" % format_ms(int(delta_t*1000))
    mecobo.close_connection()

def main():
    initialize()
    next_generation = next_generation_simple

    print("Building initial random population")
    population = []
    for i in range(POPULATION_SIZE):
        population.append(Individual())
    
    for i in range(1, N_GENERATIONS):
        population = next_generation(population, i)
        sys.stdout.flush()
        

    terminate()

main()
