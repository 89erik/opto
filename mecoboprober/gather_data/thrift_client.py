import sys
sys.path.append("gen-py/NascenseAPI_v01e")
sys.path.append("..")

#http://thrift-tutorial.readthedocs.org/en/latest/usage-example.html

import time
import traceback

import emEvolvableMotherboard
from ttypes import *
from thrift import Thrift
from thrift.transport import TSocket
from mecobo_pin_mappings import mecobo_pins_in, mecobo_single_pin_in, mecobo_pin_out, optobit

from constants import *
from binstrings import *

def init_connection():
    global client, transport
    if DEBUG:
        sys.stdout.write("Init connection...")
        sys.stdout.flush()
    try:
        transport = TSocket.TSocket(SERVER_URL, SERVER_PORT)
        transport = TTransport.TBufferedTransport(transport)

        prot = TBinaryProtocol.TBinaryProtocol(transport)
        client = emEvolvableMotherboard.Client(prot);
        transport.open();

        client.reset()
        client.clearSequences()
        if DEBUG:
            sys.stdout.write(" done\n")
            sys.stdout.flush()
    
    except Thrift.TException, tx:
        print "Connection failure"
        print tx
        exit(1)

def close_connection():
    if DEBUG:
        print "Closing connection"
    transport.close()

def queue_record_sequence(material_pin, start=REC_START, end=REC_END):
    """
    Queues recording on given pin.
    """
    mecobo_pin = mecobo_pin_out(material_pin)
    if DEBUG: 
        print "Queuing recording on material pin %d (mapped to %d)" %(material_pin, mecobo_pin)
        print "start=%d, end=%d" %(start, end)

    si = emSequenceItem()
    si.pin           = [mecobo_pin]
    si.startTime     = start
    si.endTime       = end
    si.frequency     = RECORDING_FREQUENCY
    si.operationType = emSequenceOperationType().RECORD
    si.waveformType  = emWaveFormType().PWM
    client.appendSequenceAction(si)

def queue_record_sequences(material_pins, starts=None, ends=None):
    """
    Queues recording of given material pins. These recordings can 
    be retrieved by get_recordings().
    """
    global rec_pins, wave_rec
    
    assert len(set(material_pins)) == len(material_pins) # No duplicates allowed
    assert bool(starts) == bool(ends)          # Optional args are all or nothing
    
    rec_pins = list(material_pins) # Stores in global for subsequent retrieval

    if starts:
        assert len(rec_pins) == len(starts) == len(ends)
        for i in range(len(rec_pins)):
            queue_record_sequence(rec_pins[i], start=starts[i], end=ends[i])
    else:
        for pin in rec_pins:
            queue_record_sequence(pin)

def get_recording(material_pin, wave):
    """
    Retrieves one queued recording from given pin.
    Argument wave denotes whether or not a wave (list of samples) or not a wave 
    (average of samples) are to be returned. 
    """
    mecobo_pin = mecobo_pin_out(material_pin)
    rec = client.getRecording(mecobo_pin)
    if not rec.Samples:
        raise Exception("No samples for material pin " + str(material_pin) + " (mecobo pin " + str(mecobo_pin) + ")")

    if not wave:
        acc = 0
        for sample in rec.Samples:
            acc += sample
        
        n_recs = len(rec.Samples)
        return int(round( float(acc) / float(n_recs) ))
    else:
        return rec.Samples

def get_recordings(wave=False):
    """
    Retrieves and returns all queued recordings. Can only be called once per queued recording.
    Optional argument wave denotes whether or not a wave (list of samples) or not a wave 
    (average of samples) are to be returned. Default is not wave. 
    """
    global rec_pins
    if not "rec_pins" in globals() or not rec_pins:
        raise ValueError("get_recordings() called before queue_record_sequences()")
    
    recordings = []
    for pin in rec_pins:
        recordings.append(get_recording(pin, wave))

    rec_pins = None
    return recordings

def queue_input_sequence(val, pin, wave=False, frequency=DEFAULT_FREQUENCY, start=PROBE_START, end=PROBE_END):
    """
    Queues an input sequence to mecobo on given Mecobo pin. 
    Default is constant voltage (100% or 0% duty cycle). 
    """
    mecobo_pin = mecobo_single_pin_in(pin)
    
    if val != 0 and val != 1:
        raise ValueError("Illegal val, was " + str(val) + ", expected 0 or 1.")

    if False and val == 0 and not FPGA_OPTOWALL_BIN: 
        if DEBUG: print "Ignoring pin " + str(pin)
        return

    if frequency == 0:
        val = 0
    
    si = emSequenceItem()
    si.pin           = [mecobo_pin]
    si.startTime     = start
    si.endTime       = end
    si.frequency     = frequency
    si.cycleTime     = val*50 if wave else val*100
    si.operationType = emSequenceOperationType().DIGITAL
    client.appendSequenceAction(si)

def val_on_pins(val, pins):
    """
    Transforms value to fit onto given pins.
    """
    pins = set(pins)
    
    if len(val) > N_PINS:            raise ValueError("Value cannot fit material")
    if len(val) > len(pins):         raise ValueError("Not enough pins provided")
    if OPTOWALL and pins - set(range(0, N_PINS)): raise ValueError("Pin(s) out of range")
    
    pins = list(pins)
    pins.sort()

    def take_bit():
        if val:
            return val.pop(0)
        else:
            return "0"

    if OPTOWALL:
        highest_index = N_PINS
    else:
        highest_index = pins[-1] + 1
    res = [" "] * highest_index
    for pin in pins:
        res[pin] = take_bit()
        
    return res

def queue_value(val, pins, wave=False, frequencies=None, start=PROBE_START, end=PROBE_END):
    """
    Queues a static value to Mecobo on given logical pins. 
    Value can be any binary number that will fit onto given amount of pins.
    """


    val = bin2list(val)
    val = val_on_pins(val, pins)

    pin_queue = {0: [], 1: []}

    if DEBUG:
        print "Queuing value: " + binlist2str(val)
    for bit_index, bit in enumerate(val):
        if OPTOWALL and not FPGA_OPTOWALL_BIN:
            pins = mecobo_pins_in(bit_index)
            obit = optobit(bit)

            for j in [0,1]:
                pin_queue[obit[j]].append(pins[j])
        else:
            if bit == " ":
                #bit = "0"
                continue
            pin = bit_index
            pin_queue[int(bit)].append(pin)

    if DEBUG:
        print "pins to 0: "+ str(pin_queue[0]) + "\npins to 1: " + str(pin_queue[1])
    
    if frequencies:
        assert len(frequencies) == len(pin_queue[1])
    else:
        frequencies = [DEFAULT_FREQUENCY] * len(pin_queue[1])
    
    for pin in pin_queue[0]:
        queue_input_sequence(0, pin, wave=wave, start=start, end=end)
    
    for i, pin in enumerate(pin_queue[1]):
        queue_input_sequence(1, pin, wave=wave, frequency=frequencies[i], start=start, end=end)

def run_sequences():
    """
    Runs all queued sequences and blocks until completion.
    """
    client.runSequences()
    if DEBUG: 
        sys.stdout.write("Running sequences...")
        sys.stdout.flush()
    client.joinSequences()
    if DEBUG: 
        sys.stdout.write(" done\n")
        sys.stdout.flush()
    client.clearSequences()

def test_optowall():
    assert OPTOWALL
    assert FPGA_OPTOWALL_BIN
    def within_threshold(recording, expected):
        avg = float(sum(recording)) / float(len(recording))
        
        if expected == 1:
            return avg >= 0.9
        elif expected == 0:
            return avg <= 0.1
        else:
            raise Exception("Illegal argument")
        return True

    
    raw_input("Testing optowall, place pins and press ENTER to begin...")

    errors = 0
    for repeat in range(10):
        print "Test iteration %d" %repeat
        queue_record_sequences(range(N_PINS))
        run_sequences()
        recordings = get_recordings(wave=True)
        for i, recording in enumerate(recordings):
            if not within_threshold(recording, 0):
                print "No-input recording on pin %d not within threshold of 0" % i
                print "Recorded: " + str(recording)
                errors+=1
        
        for i in range(N_PINS):
            queue_value(1, [i], wave=False)
            out_pin = i+1 if i!=15 else i-1
            queue_record_sequences([out_pin])
            run_sequences()
            recordings = get_recordings(wave=True)
            assert len(recordings) == 1
            if not within_threshold(recordings[0], 1):
                print "Input failure on pin %d" % i
                print "Recorded: " + str(recordings[0])
                errors+=1
    if errors == 0:
        print "Test OK"
        return True
    else:
        print "Test failed with %d errors" % errors
        return False




def main():
    """
    Testing-function.
    """
    print constants_report()
    try:
        init_connection()
        if False:
            test_optowall()
        else:
            endt = 30
            endr = endt
        
            #            FEDCBA9876543210        
            queue_value(0b11111111000000, range(2,16), wave=False, start=0, end=endt)
            queue_value(0b1, [1], wave=True, start=0, end=endt)
            queue_record_sequences([0], starts=[0], ends=[endr])
            run_sequences()
            rec = get_recordings(wave=True)[0]
            print rec
            print "n = %d" %len(rec)

    except:
        print traceback.format_exc()

    close_connection()
    exit(1)

if __name__ == "__main__":
    main()
