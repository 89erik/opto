#!/bin/bash

cd $(dirname $0)

THIS_DIR=$(pwd)
EXPERIMENTS=$THIS_DIR/*.json

echo Will run experiment from the following files:
for c in $EXPERIMENTS 
do
    basename $c
done 
echo

cd ..

for c in $EXPERIMENTS 
do
    echo Running experiment $c:
    ./perform_experiment.py $c --override_checklist | tee $THIS_DIR/`basename $c .json`.log
    echo
done


