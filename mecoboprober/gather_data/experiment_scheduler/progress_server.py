#! venv/bin/python
###
### Small webapp to show experiment schedule progress on a webpage
###

from flask import Flask, Response, json, request
from subprocess import Popen, PIPE, call
from time import asctime
import re
import sys
from json import loads as from_json

app = Flask(__name__)
res = {}

def single_experiment_progress(parameter_file):
    print "parameter_file = " + parameter_file
    n_probes = count = 0
    try:
        with open(parameter_file, "r") as f:
            fc = from_json(f.read())
            n_probes = (fc["LAST_PROBE_VAL"] - fc["FIRST_PROBE_VAL"]) / fc["PROBE_RANGE_STEP"]
    except:
        return (0,0)
    
    log_file = parameter_file[:-4]+"log"
    try:
        with open(log_file, "r") as f:
            pattern = re.compile("([0-1]+) -> ([0-1])")
            line = f.readline()
            while line:
                if re.match(pattern, line):
                    count += 1
                line = f.readline()
    except:
        pass
    return (count, n_probes)

template = """
<html><head><title>Experiment progress</title></head>
<body><h1>Experiment progress</h1>
%s
</body></html>
"""

@app.route("/", methods=["GET"])
def schedule_progress():
    p = Popen(["ls"], stdout=PIPE)
    rv = p.communicate()
    logs = []
    n_probes = count = 0
    for f in rv[0].split("\n"):
        if f.endswith(".json"):
            progress = single_experiment_progress(f)
            count += progress[0]
            n_probes += progress[1]
            logs.append({"name": f, "progress": progress})

    def format_fraction(count, n):
        return str(count)+"/"+str(n)+" (<b>"+str(int((float(count)/float(n))*100))+"%</b>)"

    def format_name(log):
        return "<a href=\""+log["name"]+"\">"+log["name"][:-5]+"</a>"


    res = "Total progress: "+format_fraction(count,n_probes)+"<br><ol>"
    for log in logs:
        res +="<li><a href=\""+log["name"]+"\">"+log["name"][:-5]+": "+format_fraction(log["progress"][0], log["progress"][1]) + "</a></li>"
    res += "</ol>"
    return Response(template % res)

@app.route("/<filename>", methods=["GET"])
def constants(filename):
    if not filename[-5:] == ".json":
        return "Looking for something (%s)?" % request.path, 404

    try:
        with open(filename[:-5] + ".log", "r") as f:
            return Response(f.read(), mimetype="text/plain")
    except:
        try:
            with open(filename, "r") as f:
                return Response(f.read(), mimetype="application/json")
        except:
            return "No such file \"%s\"" % request.path, 404


@app.errorhandler(404)
def not_found(e):
    return "No such file \"%s\"" % request.path, 404


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5002)
