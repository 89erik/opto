def balanced_input_vectors(VECTOR_SIZE):
    all_vectors = range(0, pow(2, VECTOR_SIZE))
    vectors = []
    for v in all_vectors:
        b = bin(v)[2:]
        b = "0" * (VECTOR_SIZE - len(b)) + b
        count = {"1": 0, "0": 0}
        for c in b:
            count[c] += 1
        if count["1"] == count["0"]:
            vectors.append(v)
    return vectors

