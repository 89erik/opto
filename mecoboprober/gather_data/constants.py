import sys
import time
import datetime
from util import format_ms
from binstrings import *
import input_vectors

DEBUG = False

# Server and HW info
SERVER_URL  = "localhost"
SERVER_PORT = 9090
HEADER      = "HNorth"
OPTOWALL    = True
FPGA_OPTOWALL_BIN = True

AMPLIFIED_PINS = range(0,16)
AMPLIFIER_RV   = "2.7k ohm"
ANALOG_VOLTAGE = 4.0

N_PINS      = 16
N_INPUTS    = 2
N_OUTPUTS   = 1
N_CONFIGS   = N_PINS - N_INPUTS - N_OUTPUTS

DEFAULT_INPUT_PINS = []
DEFAULT_OUTPUT_PINS = [0]
DEFAULT_CONFIG_PINS = list(set(range(N_PINS)) - set(DEFAULT_OUTPUT_PINS))

MATERIAL_SAMPLE = "B08S02"

# Sweep limits
FIRST_PROBE_VAL  = 0
LAST_PROBE_VAL   = pow(2, (N_CONFIGS + N_INPUTS))
PROBE_RANGE_STEP = 1
DEFAULT_INPUT_VECTORS = range(FIRST_PROBE_VAL, LAST_PROBE_VAL, PROBE_RANGE_STEP)
INPUT_VECTORS    = DEFAULT_INPUT_VECTORS #input_vectors.balanced_input_vectors(N_CONFIGS + N_INPUTS)

ALL_PIN_MAPPINGS = True #TODO this should be called "MULTIPLE_PIN_MAPPINGS"
PIN_MAPPINGS     = range(0,16)

# Single experiment parameters
PROBE_START          = 0
PROBE_END            = PROBE_START + 50
REC_START            = PROBE_START + 5
REC_END              = PROBE_END
WAVE                 = False

DEFAULT_FREQUENCY    = 1000
RECORDING_FREQUENCY  = 100000

# Overall experiment parameters
PROBE_TYPE      = "simple"
ADDITIONAL_COMMENTS = []

# Stability probe parameters
STABILITY_REPEAT  = 3

# single_simple_stable-specific parameters
DEFAULT_PROBE_VALUE = 0b000011111001010

# Non-experiment/system constants
VALID_PROBE_TYPES  = ["simple", "simple_stable", "single_stable_rating"]

def load_constants(filename):
    from json import loads as from_json
    
    with open(filename, "r") as f:
        file_content = from_json(f.read())
        for key, value in file_content.iteritems():
            if key not in globals():
                raise Exception(str(key)+" is not a recognized parameter!")
            if key[:2] != "__" and not hasattr(value, '__call__'):
                globals()[key] = value

def store_constants(filename):
    from json import dumps as to_json
    from types import ModuleType

    print "Storing following into \""+filename+"\":"
    print constants_report()

    variables = {}
    for key, value in globals().iteritems():

        if key[:2] != "__" and not hasattr(value, '__call__') and not isinstance(value, ModuleType):
            variables[key] = value

    print variables
    with open(filename, "w") as f:
        f.write(to_json(variables))


def constants_report():
    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    s  = "---[ %s ]---\n" % timestamp
    s += "Hardware configuration:\n"
    if OPTOWALL:
        s += "    * Optowall connected\n"
    else:
        s += "    * Optowall not connected\n"
    if FPGA_OPTOWALL_BIN:
        s += "    * FPGA has \"WITH_OPTOWALL\" bitfile\n"
    else:
        s += "    * FPGA has \"no_db\" bitfile\n"
   
    s += "    * Analog voltage: " + str(ANALOG_VOLTAGE) + "\n"
    s += "    * Amplified pins: " + str(AMPLIFIED_PINS) + "\n"
    s += "    * N_PINS: %d\n" % N_PINS
    s += "    * Amplifier RV resistor: %s\n" % AMPLIFIER_RV
    s += "    * Material sample: " + MATERIAL_SAMPLE + "\n"

    s += "Experiment parameters:\n"
    if WAVE:
        s += "    * Probing with square waves (50/50)\n"
    else:
        s += "    * Probing with constants (0/100)\n"
    s += "    * Input frequency %d Hz\n" % DEFAULT_FREQUENCY
    s += "    * Recording frequency %d Hz\n" % RECORDING_FREQUENCY
    s += "    * Probe type: '%s'\n" % PROBE_TYPE
    assert PROBE_TYPE in VALID_PROBE_TYPES

    if "single" in PROBE_TYPE:
        n_probes = 1
        probe_val = bin2str(DEFAULT_PROBE_VALUE, N_INPUTS+N_CONFIGS)
        probe_pins = DEFAULT_CONFIG_PINS + DEFAULT_INPUT_PINS
        probe_pins.sort()
        s += "    * Single type specifics:\n"
        s += "        * Input value: %s\n" % probe_val
        s += "        * General input pins: %s\n" % str(probe_pins)
        s += "        * Output pin%s: %s\n" % ("s"*(len(DEFAULT_OUTPUT_PINS)>1), str(DEFAULT_OUTPUT_PINS))
    else:
        n_probes = len(INPUT_VECTORS)
        if INPUT_VECTORS == DEFAULT_INPUT_VECTORS:
            assert ((LAST_PROBE_VAL - FIRST_PROBE_VAL) / PROBE_RANGE_STEP) == n_probes
            s += "    * Probing from " + str(FIRST_PROBE_VAL) + " to " + str(LAST_PROBE_VAL) + " with step " + str(PROBE_RANGE_STEP) + " (total amount is " + str(n_probes) + ")\n"
        else:
            s += "    * Probing with balanced input vectors. Total amount is: %d\n" % n_probes
        
        if ALL_PIN_MAPPINGS:
            s += "    * Pin mappings consists of all permutations with following pins as output:\n"
            s += "          " + str(PIN_MAPPINGS)+"\n"
            n_probes *= len(PIN_MAPPINGS)
            s += "    * This gives a total amount of %d probes\n" % n_probes
        else:
            s += "    * Input pins: "  + str(DEFAULT_INPUT_PINS) + "\n"
            s += "    * Output pins: " + str(DEFAULT_OUTPUT_PINS) + "\n"
            s += "    * Config pins: " + str(DEFAULT_CONFIG_PINS) + "\n"
    probe_duration = PROBE_END - PROBE_START
        
    s += "Time parameters:\n"
    s += "    * Probe start:     " + str(PROBE_START) + ", end: " + str(PROBE_END) + "\n"
    s += "    * Recording start: " + str(REC_START) + ", end: " + str(REC_END) + "\n"
    if PROBE_TYPE == "simple":
        total_duration = n_probes * probe_duration
        s += "    * Theoretical total experiment duration: ~%s\n" % format_ms(total_duration)

    elif "stable" in PROBE_TYPE:
        assert PROBE_TYPE == "simple_stable" or PROBE_TYPE == "single_stable_rating"
        total_probe_time = STABILITY_REPEAT * probe_duration * 2
        total_duration   = total_probe_time * n_probes
        s += "    * Repeats per probe: %d\n" % STABILITY_REPEAT
        s += "    * Every repeated probe is followed by a blank probe\n"
        s += "    * Total probe time: %d\n" % total_probe_time
        s += "    * Theoretical total experiment duration: ~%s\n" % format_ms(total_duration)
    est_total_duration = format_ms(int(float(total_duration) * 1.485))
    s += "    * Total experiment duration, estimated for overhead: ~%s\n" % est_total_duration
                
    if ADDITIONAL_COMMENTS:
        s+= "Additional comments:\n" 
        for comment in ADDITIONAL_COMMENTS:
            s += "    * %s\n" % comment

        
    return s

if __name__ == "__main__":
    #store_constants("experiment_scheduler/y15_const.json")
    print constants_report()

elif len(sys.argv) > 1 and sys.argv[1] and not sys.argv[1].startswith("--"):
    print "Loading constants from file \"" + sys.argv[1] + "\""
    load_constants(sys.argv[1])

