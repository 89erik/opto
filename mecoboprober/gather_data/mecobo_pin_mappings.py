from constants import *

mecobo_pins_optowall = \
    {"HWest": 
            [{"in": ["HW30", "HW27", "HW34", "HW31", "HW38", "HW35", "HW40", "HW39"], 
             "out": ["HW42", "HW41", "HW44", "HW43"]},
             {"in": [], # TODO fyll inn...
             "out": []},
             {"in": [], 
             "out": []},
             {"in": [], 
             "out": []}],
     "HNorth":
            [{"in": [8, 6, 4, 2, 1, 3, 5, 7], # o3
             "out": [17,19,21,23]},
             {"in": [10,12,14,16,18,20,22,24],# o2
             "out": [9,11,13,15]},
             {"in": [33,35,37,39,41,43,45,47],# o1
             "out": [25,27,30,31]},
             {"in": [32,34,36,38,40,42,44,46],# o0
             "out": [48,50,52,54]}]
    }

mecobo_pins_no_optowall = [0,2,4,6,8,10,12,16,18,20,22,24,26,28,30,32]

def mecobo_pins_in(material_pin):
    """
    Transforms given material pin into Mecobo input pins.
    Input is from the perspective of the material.
    """
    if OPTOWALL and not FPGA_OPTOWALL_BIN:
        local_index = material_pin % 4
        return [mecobo_pins_optowall[HEADER][material_pin/4]["in"][(local_index*2)+1] -1,
                mecobo_pins_optowall[HEADER][material_pin/4]["in"][local_index*2]     -1]
    else:
        raise Exception("Should not call mecobo_pins_in() in this HW state: OPTOWALL=%s, FPGA_OPTOWALL_BIN=%s" % (OPTOWALL, FPGA_OPTOWALL_BIN))

def mecobo_single_pin_in(material_pin):
    if OPTOWALL and not FPGA_OPTOWALL_BIN:
        return material_pin # Already handled by mecobo_pins_in()
    elif OPTOWALL and FPGA_OPTOWALL_BIN:
        return material_pin
    elif not OPTOWALL: 
        return mecobo_pins_no_optowall[material_pin]
    else:
        raise Exception("Unknown HW state: OPTOWALL=%s, FPGA_OPTOWALL_BIN=%s" % (OPTOWALL, FPGA_OPTOWALL_BIN))


def mecobo_pin_out(material_pin):
    """
    Transforms given logical optowall pin into Mecobo output pin.
    Output is from the perspective of the material.
    """
    if OPTOWALL and not FPGA_OPTOWALL_BIN:
        return mecobo_pins_optowall[HEADER][material_pin/4]["out"][material_pin%4] -1
    elif OPTOWALL and FPGA_OPTOWALL_BIN:
        return material_pin
    elif not OPTOWALL and not FPGA_OPTOWALL_BIN: 
        return mecobo_pins_no_optowall[material_pin]
    else:
        raise Exception("Unknown HW state: OPTOWALL=%s, FPGA_OPTOWALL_BIN=%s" % (OPTOWALL, FPGA_OPTOWALL_BIN))

def optobit(bit):
    """
    Transforms given input bit to a two digit value for an input optocoupler.
    """
    if bit == " ":
        return [0, 0]
    elif ((int(bit) & 1) == 1):
        return [1, 0]
    else:
        return [0, 1]

