# Install RPC library
Download and install Apache Thrift from thrift.apache.org (follow README in tar.gz), then go to lib/py and run:
sudo python setup.py install

# Generate thrift-interface
(This part has already been done and is a only note to myself or others that starts a new project.) 
Get the .thrift-description file from NASCENCE's mecobo repository (Thrift interface/NascenceAPI...thrift).
Run command:
thrift --gen py NascenceAPI...

A more thorough description exists in github/NASCENCE/mecobo/README.md

# Perform an experiment
Run the command ./perform_experiment.py. You will then be presented with a checklist. Make sure every item is OK and then press ENTER to begin the experiment or Ctrl+C to cancel. All experiment parameters are set in the constants.py file. The experiment outputs are written to stdout and are thus **not saved by default**. To save them, redirect stdout to a file, or pipe them to _tee_, which saves to file and outputs to console simultaneously. To avoid the checklist showing up in the output data, use the argument --override_checklist. In summary, the following UNIX command should be used to perform an experiment:

./perform_experiment.py --override_checklist | tee ../data/your_filename.log

To perform a genetic algorithm experiment, use the same method with the ga.py file, without the override_checklist argument. ga.py uses some parameters from constants.py, but parameters unique to the GA are located in ga.py. 

