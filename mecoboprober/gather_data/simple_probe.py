import sys
sys.path.append("gen-py/NascenseAPI_v01e")

import time
import traceback

import emEvolvableMotherboard
from ttypes import *
from thrift import Thrift
from thrift.transport import TSocket

import constants

def init_connection():
    global client, transport
    try:
        transport = TSocket.TSocket(constants.SERVER_URL, constants.SERVER_PORT)
        transport = TTransport.TBufferedTransport(transport)

        prot = TBinaryProtocol.TBinaryProtocol(transport)
        client = emEvolvableMotherboard.Client(prot);
        transport.open();

        client.reset()
        client.clearSequences()
    
    except Thrift.TException, tx:
        print "Connection failure"
        print tx
        exit(1)

def close_connection():
    transport.close()


def queue_constant(val, pin, start, end):
    print("queue_wave(" + str(pin) + ", " + str(start) + ", " + str(end) + ")")
    si               = emSequenceItem()
    si.pin           = [pin]
    si.startTime     = start
    si.endTime       = end
    si.frequency     = 1000
    si.cycleTime     = 100 * val
    si.operationType = emSequenceOperationType().DIGITAL

    client.appendSequenceAction(si)


try:
    init_connection()
    print ("Init connection done")
  
    try:
        val = int(sys.argv[1])
        pin = int(sys.argv[2])
        start = int(sys.argv[3])
        end = int(sys.argv[4])
    except:
        print("Usage: " + sys.argv[0] + " <value> <pin> <start> <end>")
        sys.exit(0)

    queue_constant(val, pin, start, end)

    client.runSequences()
    print("Running sequences...")
    client.joinSequences()
    print("...done")
    client.clearSequences()

except:
    print traceback.format_exc()

print "Closing connection"
close_connection()

