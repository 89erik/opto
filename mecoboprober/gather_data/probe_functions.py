import util
from constants import *
import thrift_client as mecobo

def simple_probe(input_pins, output_pins, config_pins, value):
    """
    A simple input/output probe, no fuss.
    """
    mecobo.queue_value(value, input_pins+config_pins, wave=WAVE)
    mecobo.queue_record_sequences(output_pins)
    mecobo.run_sequences()
    return mecobo.get_recordings(wave=False)

def avg_mean_probe(input_pins, output_pins, config_pins, value, clear_after=False):
    """
    A simple input/output probe, returns output(s) as average mean of recording(s).
    """
    
    mecobo.queue_value(value, input_pins+config_pins, wave=WAVE)
    mecobo.queue_record_sequences(output_pins)

    if clear_after:
        mecobo.queue_value(0, range(N_PINS), wave=False, start=PROBE_END+1, end=PROBE_END+5)
    
    mecobo.run_sequences()

    recordings = mecobo.get_recordings(wave=True)
    return map(util.average, recordings)

def gate_probe(input_pins, output_pins, config_pins, config_value):
    """
    Probes one configuration with all input pins permutations with a blank 
    probe in between. For instance, two input pins will probe with 
    00, 01, 10, and 11. 
    Returns a list with output from every input probe.
    """
    raise Exception("Not implemented (return value must be handled specially)")
    assert N_INPUTS == len(input_pins)

    total_period = PROBE_PERIOD * N_INPUTS * 2

    mecobo.queue_value(config_value, config_pins, wave=False, start=0, end=total_period)
    mecobo.queue_record_sequences(output_pins, starts=[0]*len(output_pins), ends=[total_period]*len(output_pins))

    assert PROBE_PERIOD >= 25

    for i in range(pow(2, N_INPUTS)):
        start = PROBE_START + i * PROBE_PERIOD * 2
        end   = start + PROBE_PERIOD
        mecobo.queue_value(i, input_pins, wave=False, start=start, end=end)
        
        start_filler = end
        end_filler   = start_filler + PROBE_PERIOD
        mecobo.queue_value(0, input_pins, wave=False, start=start_filler, end=end_filler)
    
    mecobo.run_sequences()
    recording = mecobo.get_recordings(wave=True)
    
    samples_per_group = len(recording[0]) / (N_INPUTS * 2)
    recordings = []
    for i in range(N_INPUTS):
        recordings.append(recording[i*samples_per_group:(i+1)*samples_per_group])
   
    return map(util.average, recordings)



def stable_probe(input_pins, output_pins, config_pins, value, probe_function):
    """
    Performs a probe of given type multiple times and returns all outputs in a list.
    """
    outputs = []
    for i in range(STABILITY_REPEAT):
        outputs.append(probe_function(input_pins, output_pins, config_pins, value))
    return outputs

def stable_gate_probe(input_pins, output_pins, config_pins, config_value):
    raise Exception("Not implemented")
    return stable_probe(input_pins, output_pins, config_pins, value, gate_probe)

def stable_simple_probe(input_pins, output_pins, config_pins, value, include_raw=False):
    """
    Performs a simple probe many times and returns the mean of its output
    in a floating point number x, where 0.0 >= x <= 1.0
    """
    output_recordings = stable_probe(input_pins, output_pins, config_pins, value, simple_probe)
    
    pin_recordings = zip(*output_recordings)
    
    mean = map(util.average, pin_recordings)

    if include_raw:
        return (mean, pin_recordings)
    else:
        return mean


