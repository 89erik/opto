#! /usr/bin/env python
## -*- coding: utf-8 -*-
import sys
sys.path.append("..")

import thrift_client as mecobo
from constants import *
from binstrings import *
from util import format_ms
import util

import probe_functions

import sys
import time

def select_pins(output_pin):
    input_pins  = []
    output_pins = [output_pin]
    config_pins = list(set(range(N_PINS)) - set(output_pins))
    
    return (input_pins, output_pins, config_pins)

def sweep(input_pins, output_pins, config_pins, probe):
    for val in INPUT_VECTORS:
        value = val
        res = probe(input_pins, output_pins, config_pins, value)
        print bin2str(value, N_INPUTS + N_CONFIGS) + " -> " + str(res[0])
        sys.stdout.flush()
    print ""

def print_pins(input_pins, output_pins, config_pins):
    print "input="+str(input_pins)+", config="+str(config_pins)+", output="+str(output_pins)

def confirm_checklist():
    for arg in sys.argv:
        if "--override_checklist" == arg: 
            return
    print "Checklist:"
    print "1. Mecobo has correct FPGA bitfile"
    print "2. Mecobo has red lights flashing"
    print "3. Power supplies and jumpers OK"
    print "4. No unstaged changes to src files"
    print "5. All parameters are OK (listed above)"
    print "\nThis checklist can be ignored by option --override_checklist."
    print "Press enter to continue\n"
    raw_input("")

def main():
    print constants_report()
    if DEBUG:
        print "\n-------------------\nWarning! DEBUG enabled\n-------------------\n"
    confirm_checklist()
    mecobo.init_connection()
    start_time = time.time()

    sweep_experiment = False

    if PROBE_TYPE == "simple":
        sweep_experiment = True
        probe_function = probe_functions.simple_probe
    elif PROBE_TYPE == "simple_stable":
        sweep_experiment = True
        probe_function = probe_functions.stable_simple_probe
    elif PROBE_TYPE == "single_stable_rating":
        probe = probe_functions.avg_mean_probe
        recordings = []
        for i in range(STABILITY_REPEAT):
            rec = probe(DEFAULT_INPUT_PINS, DEFAULT_OUTPUT_PINS, DEFAULT_CONFIG_PINS, DEFAULT_PROBE_VALUE, clear_after=True)
            assert len(rec) == 1
            rec = rec[0]
            print rec
            sys.stdout.flush()
            recordings.append(rec)
        
        avg = util.average(recordings)
        avg_diffs = map(lambda rec: abs(avg - rec), recordings)
        score = sum(avg_diffs) / float(len(avg_diffs))
        print "Average: %f\n" % avg
        print "Score: %f\n" % score

    else:
        raise Exception("Unknown probe type!")

    if sweep_experiment:
        if ALL_PIN_MAPPINGS:
            for output_pin in PIN_MAPPINGS:
                pins = select_pins(output_pin)
                print_pins(*pins)
                sweep(*pins, probe=probe_function)
        else:
            sweep(DEFAULT_INPUT_PINS, DEFAULT_OUTPUT_PINS, DEFAULT_CONFIG_PINS)

    total_time = time.time() - start_time
    print "\nTotal experiment time: %s" % format_ms(int(total_time*1000))
    mecobo.close_connection()

if __name__ == "__main__":
    main()

